#Importing numpy for array modification
import numpy as np
#Importing TensorFlow for making the data usable by Neural Networks
import tensorflow as tf

class TrainingData:

    def __init__(self, x, y, test_x, test_y):
        
        #Storing x,y,test_x and test_y in the class
        self.x = x
        self.y = y
        self.test_x = test_x
        self.test_y = test_y
    
    def reshape_x(self, shape, datatype):
        
        shape[0] = len(self.x) #Changing the first integer of the shape to match size of array
        self.x = np.reshape(self.x, shape) #Reshaping the array using the shape
        
        shape[0] = len(self.test_x) #Changing the first integer of the shape to match size of array
        self.test_x = np.reshape(self.test_x, shape) #Reshaping the array using the shape
        
        #Changing the type of our data
        self.x = self.x.astype(datatype)
        self.test_x = self.test_x.astype(datatype)
        
    def reshape_y(self, shape, datatype):
        
        shape[0] = len(self.y) #Changing the first integer of the shape to match size of array
        self.y = np.reshape(self.y, shape) #Reshaping the array using the shape
        
        shape[0] = len(self.test_y) #Changing the first integer of the shape to match size of array
        self.test_y = np.reshape(self.test_y, shape) #Reshaping the array using the shape
        
        #Changing the type of our data
        self.y = self.y.astype(datatype)
        self.test_y = self.test_y.astype(datatype)
        
    def pixel_to_float(self, x=True, y=False):
        
        #If needed, divide the x by 255
        if (x == True):
            self.x /= 255
            self.test_x /= 255
            
        #If needed, divide the y by 255
        if (y == True):
            self.y /= 255
            self.test_y /= 255
    
    def create_y_classes(self, classes, datatype):
        
        #Making the y and test_y categorical (so it can be used as a label in classification tasks)
        self.y = tf.keras.utils.to_categorical(self.y, classes)
        self.test_y = tf.keras.utils.to_categorical(self.test_y, classes)
        
        #Changing the type of the y and test_y
        self.y = self.y.astype(datatype)
        self.test_y = self.test_y.astype(datatype)
    
    #Finally export the training data as a TensorFlow dataset
    def create(self, batch_size, shuffle_size):
        
        self.dataset = tf.data.Dataset.from_tensor_slices((self.x, self.y)) #Create the dataset from x and y
        self.dataset = self.dataset.shuffle(shuffle_size) #Shuffle the dataset
        self.dataset = self.dataset.batch(batch_size) #Cut the dataset into different batches
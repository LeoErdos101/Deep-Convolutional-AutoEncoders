# Importing tensorflow for ML
import tensorflow as tf
# Importing NumPy for reshaping arrays
import numpy as np
# Importing openCV
import cv2
# Importing OS for OS related stuff
import sys

# Get our model from deep-conv-autoencoder.h5
model = tf.keras.models.load_model("deep-conv-autoencoder.h5")

# Compile our model
model.compile(loss="binary_crossentropy",
             optimizer=tf.train.RMSPropOptimizer(learning_rate=0.001),
             metrics=["accuracy"])

img = []

if (len(sys.argv) != 2): # Check if there are 2 specified arguments
    print("Wrong Usage. The Usage is: python use.py <IMG PATH>")
    sys.exit() # Quit the program
else:
    try:
        img = cv2.imread(sys.argv[1], 0) # Load the image in img
        img = np.reshape(img, (1, 28, 28, 1)) # Reshape the image
        img = img.astype(np.float32) # Change the type of the image
    except:
        print("The path specified does not contain a jpg image")
        print("The image has to be a 28x28 image")
        sys.exit()

predicted_image = model.predict(img)
predicted_image = np.reshape(predicted_image, (1, 28, 28, 1))

cv2.imshow("Predicted Image", predicted_image)
cv2.imwrite("Converted/" + sys.argv[1], predicted_image.tolist())
#Importing TensorFlow for Deep Learning
import tensorflow as tf
#Importing Numpy for Maths
import numpy as np
#Importing Matplotlib for displaying images
import matplotlib.pyplot as plt
#Importing random for randomness
import random
#We import our totrainingdata file to convert our data to training data
import totrainingdata as ttd

tf.enable_eager_execution()


# loading MNIST data from the keras datasets
(train_x, _), (test_x, _) = tf.keras.datasets.mnist.load_data()

# Creating a function to put a random 4x4 white square on images it gets
def whiteSquare(images):
  
  # Creates a numpy array of images we are going to modify
  newImages = np.array(images)
  
  for image in newImages: # Loops through the new images
    
    # Creates the random starting position of the white square
    start_x = random.randint(0, 24)
    start_y = random.randint(0, 24)
    
    # Makes all pixels present in the square white
    for pixel in image[start_y:start_y+4]:
      pixel[start_x:start_x+4] = 255
      
  return newImages # Returns the images to us

#We use the function to create a set of images which have a white square on them
white_squared_images_train = whiteSquare(train_x)
white_squared_images_test = whiteSquare(test_x)


dataset = ttd.TrainingData(white_squared_images_train, train_x, white_squared_images_test, test_x) #Initialize the dataset

#Reshaping Training Data
dataset.reshape_x([None, 28, 28, 1], np.float32)
dataset.reshape_y([None, 784], np.float32)

dataset.pixel_to_float(x=True, y=True) #Converting pixel values to floats

# Creating the Sequential Model
model = tf.keras.Sequential()

# Creating the Convolutional Encoder
model.add(tf.keras.layers.Conv2D(filters=64, activation=tf.nn.relu, kernel_size=(3, 3), strides=1, input_shape=(28, 28, 1,)))
model.add(tf.keras.layers.MaxPooling2D((2, 2), strides=2))
model.add(tf.keras.layers.Conv2D(filters=32, activation=tf.nn.relu, kernel_size=(3, 3), strides=1))
model.add(tf.keras.layers.MaxPooling2D((2, 2), strides=2))

# Making the bottleneck
model.add(tf.keras.layers.Conv2D(filters=3, activation=tf.nn.relu, kernel_size=(3, 3), strides=1))

# Creating the Convolutional Decoder
model.add(tf.keras.layers.UpSampling2D((2, 2)))
model.add(tf.keras.layers.Conv2D(filters=32, activation=tf.nn.relu, kernel_size=(3, 3), strides=1))
model.add(tf.keras.layers.UpSampling2D((2, 2)))
model.add(tf.keras.layers.Conv2D(filters=64, activation=tf.nn.relu, kernel_size=(3, 3), strides=1))

# Creating the fully-connected layer which can display the image
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(784, activation=tf.nn.relu))

# Defining the RMSProp Optimizer
rmsProp = tf.train.RMSPropOptimizer(0.001)

# Compiling our model
model.compile(loss="binary_crossentropy",
             optimizer=rmsProp,
             metrics=["accuracy"])
             
epochs = 70

# Training our model
model.fit(x=dataset.x, y=dataset.y, validation_data=(dataset.test_x, dataset.test_y),
         epochs=epochs, batch_size=300)

# Saving our model
model.save("deep-conv-autoencoder.h5")

